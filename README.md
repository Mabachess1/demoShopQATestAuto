# Homemade framework tools

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

Example CI|CD tools about automation on the DemoShopQA website - POEC 2021 Testeur Logiciel
## Start

This is some tools and frameworks for automation testing.
Try to orchestrate the stack & CI|CD from Gitlab
### Prerequisites

- Gitlab account 
- Your own Gitlab-runner
- SonarQube : _latest docker version_
- Docker
- IDE : _like IntelliJ or other_
- Maven : _latest_
- Java JDK 8 or 11
### Installation

_in progress_

## Begin

_in progress_ 

## Fabriqué avec

_exemples :_

- [Gitlab](https://gitlab.com) - CI|CD pipeline 
- [Docker](https://www.docker.com/products/docker-desktop) - The fastest way to containerize applications on your desktop
- [Sonarqube](https://hub.docker.com/_/sonarqube) - SonarQube is an open source platform for continuous inspection of code quality.


## Contributing

_in progress_

## Versions

**v.1.0**
## Auteurs

- **Mabachess (M7s)** _alias_ [@mabachess1](https://gitlab.com/mabachess1)

[contributors](https://gitlab.com/Mabachess1/demoShopQATestAuto/-/graphs/main) to see who help to this project!
## License

Project on  `MIT` - see this file [LICENSE.md](LICENSE.md) for more informations.
